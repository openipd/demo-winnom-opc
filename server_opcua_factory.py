import sys
sys.path.insert(0, "..")
import time


from opcua import ua, Server


if __name__ == "__main__":

    # setup our server
    server = Server()
    server.set_endpoint("opc.tcp://0.0.0.0:4840/freeopcua/server/")

    # setup our own namespace, not really necessary but should as spec
    uri = "http://examples.freeopcua.github.io"
    idx = server.register_namespace(uri)

    # get Objects node, this is where we should put our nodes
    objects = server.get_objects_node()

    # populating our address space
    myobj = objects.add_object(idx, "MyObject")
    myvar = myobj.add_variable(idx, "Fill Valve", 0.0)
    myvar.set_writable()    # Set MyVariable to be writable by clients
    myvar1 = myobj.add_variable(idx, "Discharge Valve", 0.0)
    myvar1.set_writable()    # Set MyVariable to be writable by clients
    myvar2 = myobj.add_variable(idx, "FACTORY PAUSE", False)
    myvar2.set_writable()    # Set MyVariable to be writable by clients
    myvar3 = myobj.add_variable(idx, "FACTORY RUN", False)
    myvar3.set_writable()    # Set MyVariable to be writable by clients
    myvar4 = myobj.add_variable(idx, "FACTORY RESET", False)
    myvar4.set_writable()    # Set MyVariable to be writable by clients
    myvar5 = myobj.add_variable(idx, "Level Meter ", 0.0)
    myvar5.set_writable()    # Set MyVariable to be writable by clients
    myvar6 = myobj.add_variable(idx, "Flow Meter ", 0.0)
    myvar6.set_writable()    # Set MyVariable to be writable by clients
    myvar7 = myobj.add_variable(idx, "SetPoint ", 0.0)
    myvar7.set_writable()    # Set MyVariable to be writable by clients



    # starting!
    server.start()
    
    try:
        count = 0
        while True:
            time.sleep(1)
            count += 0.1
    #        myvar.set_value(count)
    finally:
        #close connection, remove subcsriptions, etc
        server.stop()
