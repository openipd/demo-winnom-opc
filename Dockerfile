FROM python:3.7.5

RUN apt install -y libssl-dev libffi-dev

ADD server_opcua_factory.py /

RUN pip install cryptography python-dateutil lxml pytz pyopenssl opcua

EXPOSE  4840

CMD [ "python", "./server_opcua_factory.py"]
